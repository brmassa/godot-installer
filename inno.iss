[Setup]
AppId={{0F694364-35AC-4CFB-91B5-3482CDB3EBAF}
AppName=Godot
AppVersion=3
AppVerName=Godot 3
DefaultDirName={pf}\Godot
DisableProgramGroupPage=yes
OutputBaseFilename=godot_setup
ChangesAssociations = yes
Compression=none

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

; Simply copy the contents of the current folder into the install path (including the installer)
[Files]
Source: "{src}\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs  external

; Create menu items
[Icons]
Name: "{commonprograms}\Godot\Godot"; Filename: "{app}\godot.exe"
Name: "{commonprograms}\Godot\Uninstall"; Filename: "{uninstallexe}"

; Associate .GODOT
[Registry]
Root: HKCR; Subkey: ".godot"; ValueType: string; ValueName: ""; ValueData: "Godot"; Flags: uninsdeletevalue
Root: HKCR; Subkey: "Godot"; ValueType: string; ValueName: ""; ValueData: "Godot Project"; Flags: uninsdeletekey
Root: HKCR; Subkey: "Godot\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\godot.exe,0"
Root: HKCR; Subkey: "Godot\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\godot.exe"" ""%1"""

; Since the Godot devs name the EXE differently on EACH release, make a copy of the executable as "godot.exe"
[Code]
function GetFileName(const AFileName: string): string;
begin
  Result := ExpandConstant('{src}\' + AFileName);
end;

function FileFileWildcard(const FileName: string): String;
var
  FindRec: TFindRec;
begin
  Result := '';
  if FindFirst(FileName, FindRec) then
  try
    if FindRec.Attributes and FILE_ATTRIBUTE_DIRECTORY = 0 then
      Result := FindRec.Name;
  finally
    FindClose(FindRec);
  end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
  if (CurStep = ssInstall ) then
  begin
  FileCopy(GetFileName(FileFileWildcard(GetFileName('Godot_v*.exe'))), GetFileName('godot.exe'), false);
  end;
end;

